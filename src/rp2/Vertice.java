package rp2;

/**
 * Classe responsável por criar objetos de tipo Vertice.
 * @author Bruno R, Joao A, Michel S, Gustavo R
 */
public class Vertice {
    private double x;
    private double y;
    
    /**
    * Construtor que recebe dois valores referentes às coordenadas
    * x e y do vértice.
    * @param x coordenada x do vertice
    * @param y coordenada y do vertice
    */    
    public Vertice (double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }    
}
