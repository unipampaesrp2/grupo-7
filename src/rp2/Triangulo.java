
package rp2;

/**
 * Classe que cria um triangulo
 *
 * @author Joao Aurelio
 */
public class Triangulo extends PoligonoRegular {

    double altura;

    public Triangulo(double tLado, String CorBorda, String CorPreenchimento, Vertice central) {
        super(3, tLado, CorBorda, CorPreenchimento, central);

    }

}
