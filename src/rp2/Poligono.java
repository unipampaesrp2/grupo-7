/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rp2;

/**
 *Classe abstrata que contém métodos abstratos que são estendidos para as figuras que a herdam;
 * 
 * @author Michel M.
 */
abstract class Poligono {

    protected int nLados;
    protected double tLado;
    protected Vertice vertices[];
    protected Vertice central;
    protected Aresta arestas[];
    protected String CorBorda;
    protected String CorPreenchimento;

    public Poligono(int nLados, double tLado, Vertice central, String CorBorda, String CorPreenchimento) {
        this.nLados = nLados;
        this.tLado = tLado;
        this.central = central;
        this.CorBorda = CorBorda;
        this.CorPreenchimento = CorPreenchimento;
    }

    abstract String getCorBorda();

    abstract void setCorBorda(String CorBorda);

    abstract String getCorPreenchimento();

    abstract void setCorPreenchimento(String CorPreenchimento);

    abstract int getnLados();

    abstract Aresta getAresta(int i);

    abstract double getVerticeX(int i);

    abstract double getVerticeY(int i);
    
    /**
     * Calcula o perímetro da figura;
     * @return O Perímetro da figura;
     */
    abstract double calculaPerimetro();
    
    /**
     * Calcula a área da figura;
     * @return A área da figura;
     */
    abstract double calculaArea();
    
    /**
    * Desloca a figura na Horizontal;
    * @param tamDeslocamento tamanho do deslocamento da figura;
    */
    abstract void deslocamentoHorizontal(double tamDeslocamento);
    
    /**
    * Desloca a figura na Vertical;
    * @param tamDeslocamento tamanho do deslocamento da figura;
    */
    abstract void deslocamentoVertical(double tamDeslocamento);
    
    /**
     * Método que faz a figura rotacionar no sentido anti-horário em graus sobre o mesmo centro;
     * @param rotacao quantidade de graus que a figura deve rotacionar;
     */
    abstract void rotacao(double rotacao);
    
    /**
     * Aumenta proporcionalmente a figura;
     * @param valor número de vezes que a figura irá aumentar;
     */
    abstract void aumentarTamanho(double valor);    
    
    /**
     * Diminui proporcionalmente a figura;
     * @param valor número de vezes que a figura irá diminuir;
     */
    abstract void diminuirTamanho(double valor);
}
