/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rp2;

/**
 *Classe que instancia o pentagono.
 * @author Michel M.
 */
public class Pentagono extends PoligonoRegular{

    public Pentagono( double tLado, String CorBorda, String CorPreenchimento, Vertice central) {
        super(5, tLado, CorBorda, CorPreenchimento, central);
    }
}
