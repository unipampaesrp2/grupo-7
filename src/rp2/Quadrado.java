/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rp2;

/**
 * Classe que instancia o quadrado;
 * @author Michel M.
 */
public class Quadrado extends PoligonoRegular{

    public Quadrado( double tLado, String CorBorda, String CorPreenchimento, Vertice central) {
        super(4, tLado, CorBorda, CorPreenchimento, central);
    }
}
