
package rp2;

/**
 * Classe abstrata que contém métodos que calculam area, perimetro, encontram vértices e arestas para qualquer figura regular;
 * 
 * @author Bruno R. , Gustavo , Michel M.
 */
abstract class PoligonoRegular extends Poligono{

    public PoligonoRegular(int nLados, double tLado, String CorBorda, String CorPreenchimento, Vertice central) {
        super(nLados, tLado, central, CorBorda, CorPreenchimento);
        this.nLados = nLados;
        this.tLado = tLado;
        this.vertices = new Vertice[this.nLados];
        this.central = central;
        this.arestas = new Aresta[this.nLados];
        this.CorBorda = CorBorda;
        this.CorPreenchimento = CorPreenchimento;
        double raio = calculaRaio(this.tLado, this.nLados);
        determinaVertices(raio);
        determinaAresta();
    }

    @Override
    public String getCorBorda() {
        return CorBorda;
    }

    @Override
    public void setCorBorda(String CorBorda) {
        this.CorBorda = CorBorda;
    }

    @Override
    public String getCorPreenchimento() {
        return CorPreenchimento;
    }

    @Override
    public void setCorPreenchimento(String CorPreenchimento) {
        this.CorPreenchimento = CorPreenchimento;
    }

    @Override
    public int getnLados() {
        return nLados;
    }


    @Override
    public Aresta getAresta(int i) {
        return arestas[i];
    }

    @Override
    public double getVerticeX(int i) {
        return vertices[i].getX();
    }

    @Override
    public double getVerticeY(int i) {
        return vertices[i].getY();
    }

    public void setVertice(int i, Vertice vertice) {
        this.vertices[i] = vertice;
    }

    public double gettLado() {
        return tLado;
    }

    public void settLado(double tLado) {
        this.tLado = tLado;
    }

    /**
     * Determina os vértices da figura de acordo com a rotação, usando o ângulo central;
     * @param raio Necessário para encontrar o ângulo central;
     */
    private void determinaVertices(double raio) {
        double auxGraus = determinaGrausFigura(nLados);
        double rotacaoAtual = (Math.PI * 3) / 2 - (auxGraus / 2);       
        for (int cont = 0; cont < this.nLados; cont++) {
            this.vertices[cont] = new Vertice(central.getX() + raio * Math.cos(rotacaoAtual), central.getY() + raio * Math.sin(rotacaoAtual));           
            rotacaoAtual += auxGraus;
        }
    }
    
    /**
     * Encontra o ângulo de cada vértice da figura regular
     * @param nLados número da lados da figura
     * @return 
     */
    public static double determinaGrausFigura(int nLados) {
        return 2 * Math.PI / nLados;
    }
    
    /**
     * Converte graus da figura em radianos
     * @param grau
     * @return Grau convertidos em radiano
     */
    public static double converteGrausEmRadianos(double grau){
        return (grau*Math.PI)/180;
    }

    @Override
    public double calculaPerimetro() {
        return nLados * tLado;
    }

    /**
     * Método que calcula o raio; Fórmula usada retirada do wikipédia em http://pt.wikipedia.org/wiki/Pol%C3%ADgono_regular     *
     * @param tLado tamanho do lado;
     * @param nLados número de lados da figura;
     * @return raio;
     */
    public static double calculaRaio(double tLado, int nLados) {
        return tLado / (2 * Math.sin(Math.PI / nLados));
    }

    @Override
    public double calculaArea() {
        double apotema = tLado / (2 * Math.tan(Math.PI / nLados));
        //System.out.println(" Apotema "+apotema);
        return (calculaPerimetro() * apotema)/2;
    }
    
    /**
     * Determina as arestas da figura;
     */
    private void determinaAresta() {
        for (int cont = 0; cont < nLados; cont++) {
            try {
                arestas[cont] = new Aresta(vertices[cont], vertices[cont + 1]);
            } catch (Exception e) {
                arestas[cont] = new Aresta(vertices[cont], vertices[0]);
            }
        }
    }

    @Override
    public void deslocamentoHorizontal(double tamDeslocamento) {
        double x = central.getX();
        central.setX(x + tamDeslocamento);
        determinaVertices(calculaRaio(this.tLado, this.nLados));

    }

    @Override
    public void deslocamentoVertical(double tamDeslocamento) {
        double y = central.getY();
        central.setY(y + tamDeslocamento);
        determinaVertices(calculaRaio(this.tLado, this.nLados));
    }

    @Override
    void rotacao(double rotacao) {
        double raio = calculaRaio(tLado, nLados);
        double auxGraus = determinaGrausFigura(nLados) + converteGrausEmRadianos(rotacao);
        double rotacaoAtual = (Math.PI * 3) / 2 - (auxGraus / 2);   //  calculo para começar no ponto mais a esquerda e abaixo no plano;      
        for (int cont = 0; cont < this.nLados; cont++) {
            this.vertices[cont] = new Vertice(central.getX() + raio * Math.cos(rotacaoAtual), central.getY() + raio * Math.sin(rotacaoAtual));
            rotacaoAtual += auxGraus;
        }
    }
    
    @Override
    public void aumentarTamanho(double valor){
        settLado(this.tLado*valor);
        determinaVertices(calculaRaio(tLado, nLados));
        determinaAresta();        
    }
    
    @Override
    public void diminuirTamanho(double valor){
        settLado(this.tLado/valor);
        determinaVertices(calculaRaio(tLado, nLados));
        determinaAresta();        
    }
}
