/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rp2;

import java.util.Scanner;

/**
 * Menu interativo;
 *
 * @author Bruno R. , Gustavo , Michel M.
 */
public class Rp2 {

    /**
     * @param args the command line arguments
     */
    static Vertice central = new Vertice(500, 500);
    static Scanner ent = new Scanner(System.in);

    public static void main(String[] args) {

        int menu = -90;
        for (int cont = 0; menu != 0; cont++) {
            System.out.println(" DIGITE:"
                    + "\n |1| TRIANGULO"
                    + "\n |2| QUADRADO"
                    + "\n |3| RETANGULO"
                    + "\n |4| PENTAGONO"
                    + "\n |5| HEXAGONO"
                    + "\n |0| SAIR");
            menu = ent.nextInt();
            switch (menu) {
                case 1:
                    criaTriangulo();
                    break;
                case 2:
                    criaQuadrado();
                    break;
                case 3:
                    criaRetangulo();
                    break;
                case 4:
                    criaPentagono();
                    break;
                case 5:
                    criaHexagono();
                    break;
            }
        }
    }

    public static void criaQuadrado() {
        double tam;
        System.out.println(" Digite o Tamanho do lado da figura");
        tam = ent.nextDouble();
        Quadrado q = new Quadrado(tam, "Azul", "Branco", central);
        System.out.println("  Area     " + q.calculaArea());
        System.out.println("  Perimetro " + q.calculaPerimetro());
        System.out.println("  VÉRTICES  ");
        for (int i = 0; i < q.getnLados(); i++) {
            System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
        }
        System.out.println("Deseja rotacionar a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int n = ent.nextInt();
        if (n == 1) {
            System.out.println("Digite quantos graus rotacionar:");
            double rotacao = ent.nextDouble();
            q.rotacao(rotacao);
            for (int i = 0; i < q.getnLados(); i++) {
                System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
            }
        }
        System.out.println("Deseja aumentar ou diminuir a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int e = ent.nextInt();
            if (e == 1) {
                System.out.println("Digite 1 para Aumentar , 2 para Diminuir, 0 para Sair");
                int a = ent.nextInt();
                if (a == 1) {
                    System.out.println("Digite o quanto voce quer aumentar proporcionalmente:");
                    double aumenta = ent.nextDouble();
                    q.aumentarTamanho(aumenta);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                }
                }else if (a == 2){
                    System.out.println(" Digite o quanto voce quer reduzir proporcionalmente");
                    double diminui = ent.nextDouble();
                    q.diminuirTamanho(diminui);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                    }
                }else{                    
                }
            }else{                
            }
    }
    
    public static void criaRetangulo() {
        double base;
        System.out.println(" Digite o Tamanho do lado da figura");
        base = ent.nextDouble();
        double altura;
        System.out.println(" Digite a altura da figura");
        altura = ent.nextDouble();
        Retangulo q = new Retangulo(base, altura, "Azul", "Branco", central);
        System.out.println("  Area     " + q.calculaArea());
        System.out.println("  Perimetro " + q.calculaPerimetro());
        q.determinaVertices();
        System.out.println("  VÉRTICES  ");
        for (int i = 0; i < q.getnLados(); i++) {
            System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
        }
        System.out.println("Deseja rotacionar a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int n = ent.nextInt();
        if (n == 1) {
            System.out.println("Digite quantos graus rotacionar:");
            double rotacao = ent.nextDouble();
            q.rotacao(rotacao);
            for (int i = 0; i < q.getnLados(); i++) {
                System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
            }
        }
        System.out.println("Deseja aumentar ou diminuir a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int e = ent.nextInt();
            if (e == 1) {
                System.out.println("Digite 1 para Aumentar , 2 para Diminuir, 0 para Sair");
                int a = ent.nextInt();
                if (a == 1) {
                    System.out.println("Digite o quanto voce quer aumentar proporcionalmente:");
                    double aumenta = ent.nextDouble();
                    q.aumentarTamanho(aumenta);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                }
                }else if (a == 2){
                    System.out.println(" Digite o quanto voce quer reduzir proporcionalmente");
                    double diminui = ent.nextDouble();                    
                    q.diminuirTamanho(diminui);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                    }
                }else{                    
                }
            }else{                
            }
    }

    public static void criaTriangulo() {
        double tam;
        System.out.println(" Digite o Tamanho do lado da figura");
        tam = ent.nextDouble();
        Triangulo q = new Triangulo(tam, "Preto", "Branco", central);
        System.out.println("  Area     " + q.calculaArea());
        System.out.println("  Perimetro " + q.calculaPerimetro());
        System.out.println("  VÉRTICES  ");
        for (int i = 0; i < q.getnLados(); i++) {
            System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
        }
        System.out.println("Deseja rotacionar a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int n = ent.nextInt();
        if (n == 1) {
            System.out.println("Digite quantos graus rotacionar:");
            double rotacao = ent.nextDouble();
            q.rotacao(rotacao);            
            for (int i = 0; i < q.getnLados(); i++) {
                System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
            }
        }
        System.out.println("Deseja aumentar ou diminuir a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int e = ent.nextInt();
            if (e == 1) {
                System.out.println("Digite 1 para Aumentar , 2 para Diminuir, 0 para Sair");
                int a = ent.nextInt();
                if (a == 1) {
                    System.out.println("Digite o quanto voce quer aumentar proporcionalmente:");
                    double aumenta = ent.nextDouble();
                    q.aumentarTamanho(aumenta);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                }
                }else if (a == 2){
                    System.out.println(" Digite o quanto voce quer reduzir proporcionalmente");
                    double diminui = ent.nextDouble();
                    q.diminuirTamanho(diminui);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                    }
                }else{                    
                }
            }else{                
            }
    }

    public static void criaPentagono() {
        double tam;
        System.out.println(" Digite o Tamanho do lado da figura");
        tam = ent.nextDouble();
        Pentagono q = new Pentagono(tam, "Azul", "Branco", central);
        System.out.println("  Area     " + q.calculaArea());
        System.out.println("  Perimetro " + q.calculaPerimetro());
        System.out.println("  VÉRTICES  ");
        for (int i = 0; i < q.getnLados(); i++) {
            System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
        }
        System.out.println("Deseja rotacionar a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int n = ent.nextInt();
        if (n == 1) {
            System.out.println("Digite quantos graus rotacionar:");
            double rotacao = ent.nextDouble();
            q.rotacao(rotacao);
            for (int i = 0; i < q.getnLados(); i++) {
                System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
            }
        }
        System.out.println("Deseja aumentar ou diminuir a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int e = ent.nextInt();
            if (e == 1) {
                System.out.println("Digite 1 para Aumentar , 2 para Diminuir, 0 para Sair");
                int a = ent.nextInt();
                if (a == 1) {
                    System.out.println("Digite o quanto voce quer aumentar proporcionalmente:");
                    double aumenta = ent.nextDouble();
                    q.aumentarTamanho(aumenta);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                }
                }else if (a == 2){
                    System.out.println(" Digite o quanto voce quer reduzir proporcionalmente");
                    double diminui = ent.nextDouble();
                    q.diminuirTamanho(diminui);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                    }
                }else{                    
                }
            }else{                
            }
    }

    public static void criaHexagono() {
        double tam;
        System.out.println(" Digite o Tamanho do lado da figura");
        tam = ent.nextDouble();
        Hexagono q = new Hexagono(tam, "Azul", "Branco", central);
        System.out.println("  Area     " + q.calculaArea());
        System.out.println("  Perimetro " + q.calculaPerimetro());
        System.out.println("  VÉRTICES  ");
        for (int i = 0; i < q.getnLados(); i++) {
            System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
        }
        System.out.println("Deseja rotacionar a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int n = ent.nextInt();
        if (n == 1) {
            System.out.println("Digite quantos graus rotacionar:");
            double rotacao = ent.nextDouble();
            q.rotacao(rotacao);
            for (int i = 0; i < q.getnLados(); i++) {
                System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
            }
        }        
        
        System.out.println("Deseja aumentar ou diminuir a figura?");
        System.out.println(" 1 para Sim , 0 para Nao");
        int e = ent.nextInt();
            if (e == 1) {
                System.out.println("Digite 1 para Aumentar , 2 para Diminuir, 0 para Sair");
                int a = ent.nextInt();
                if (a == 1) {
                    System.out.println("Digite o quanto voce quer aumentar proporcionalmente:");
                    double aumenta = ent.nextDouble();
                    q.aumentarTamanho(aumenta);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                }
                }else if (a == 2){
                    System.out.println(" Digite o quanto voce quer reduzir proporcionalmente");
                    double diminui = ent.nextDouble();
                    q.diminuirTamanho(diminui);
                    System.out.println("Área " + q.calculaArea());    
                    System.out.println("Perímetro " + q.calculaPerimetro());
                    for (int i = 0; i < q.getnLados(); i++) {
                    System.out.println(" " + q.getVerticeX(i) + "     " + q.getVerticeY(i));
                    }
                }else{                    
                }
            }else{                
            }
    }
}
