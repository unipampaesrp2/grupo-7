/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rp2;

/**
 * Classe que instancia o retangulo;
 * @author Gustavo Rodrigues
 */
public class Retangulo extends Poligono {

    double altura;
    double anguloDoPrimeiroPonto;

    public Retangulo(double tLado, double altura, String CorBorda, String CorPreenchimento, Vertice inicial) {
        super(4, tLado, inicial, CorBorda, CorPreenchimento);
        this.altura = altura;
        anguloDoPrimeiroPonto = Math.atan((altura / 2) / (tLado / 2));
        vertices = new Vertice[nLados];
        arestas = new Aresta[nLados];
        determinaVertices();
        determinaAresta();
    }

    /**
     * Cria-se uma variável para armazenar a informação de base e efetua o
     * cálculo do perímetro do retângulo
     *
     * @return resultado do cálculo do perímetro do retângulo
     */
    @Override
    public double calculaPerimetro() {
        return altura * 2 + 2 * tLado;
    }

    /**
     * Cria-se uma variável para armazenar a informação de base e efetua o
     * cálculo da área do retângulo
     *
     * @return resultado do cálculo da área do retângulo
     */
    @Override
    public double calculaArea() {
        return tLado * altura;
    }

    /**
     * Classe que determina os vértices do retangulo;
     */
    public void determinaVertices() {
        double tangente = (altura / 2) / (tLado / 2);      // valor da tangente do angulo central;
        double angLateral = 2 * Math.atan(tangente);             // valor em radianos;
        double angSuperior = Math.PI - (angLateral);      // pi = 180   logo   pi - 2 X ang inferior;
        double raio = calculaRaio();
        double auxPosicao = anguloDoPrimeiroPonto;
        vertices[0] = new Vertice(central.getX() + (raio * Math.cos(auxPosicao)), central.getY() + (raio * Math.sin(auxPosicao)));
        auxPosicao += angSuperior;
        vertices[1] = new Vertice(central.getX() + (raio * Math.cos(auxPosicao)), central.getY() + (raio * Math.sin(auxPosicao)));
        auxPosicao += angLateral;
        vertices[2] = new Vertice(central.getX() + (raio * Math.cos(auxPosicao)), central.getY() + (raio * Math.sin(auxPosicao)));
        auxPosicao += angSuperior;
        vertices[3] = new Vertice(central.getX() + (raio * Math.cos(auxPosicao)), central.getY() + (raio * Math.sin(auxPosicao)));
        auxPosicao += angLateral / 2;
        //  System.out.println("aux    "+Math.toDegrees(auxPosicao));
        //    System.out.println("  "+Math.PI*2);
    }

    /**
     * Método que calcula o raio;
     *
     * @return
     */
    public double calculaRaio() {
        return Math.sqrt(Math.pow(tLado / 2, 2) + Math.pow(altura / 2, 2));
    }

   
    @Override
    void rotacao(double rotacao) {
        anguloDoPrimeiroPonto += Math.toDegrees(rotacao);
        determinaVertices();
    }

    @Override
    String getCorBorda() {
        return CorBorda;
    }

    @Override
    void setCorBorda(String CorBorda) {
        this.CorBorda = CorBorda;
    }

    public void settLado(double tLado) {
        this.tLado = tLado;
    }
    
    @Override
    String getCorPreenchimento() {
        return CorPreenchimento;
    }

    @Override
    void setCorPreenchimento(String CorPreenchimento) {
        this.CorPreenchimento = CorPreenchimento;
    }

    @Override
    int getnLados() {
        return nLados;
    }


    @Override
    Aresta getAresta(int i) {
       return arestas[i]; 
    }

    @Override
    double getVerticeX(int i) {
       return vertices[i].getX(); 
    }

    @Override
    double getVerticeY(int i) {
        return vertices[i].getY();
    }

    @Override
    void deslocamentoHorizontal(double tamDeslocamento) {
        double x = central.getX();
        central.setX(x + tamDeslocamento);
        determinaVertices();
    }

    @Override
    void deslocamentoVertical(double tamDeslocamento) {
        double y = central.getY();
        central.setY(y + tamDeslocamento);
        determinaVertices();
    }

    @Override
    void aumentarTamanho(double valor) {
        settLado(this.tLado*valor);
        altura = altura*valor;
        determinaVertices();
        determinaAresta();         
    }

    @Override
    void diminuirTamanho(double valor) {
        settLado(this.tLado/valor);
        altura = altura/valor;
        determinaVertices();
        determinaAresta(); 
    }
    
    private void determinaAresta() {
        for (int cont = 0; cont < nLados; cont++) {
            try {
                arestas[cont] = new Aresta(vertices[cont], vertices[cont + 1]);
            } catch (Exception e) {
                arestas[cont] = new Aresta(vertices[cont], vertices[0]);
            }
        }
    }
    
}
