package rp2;

/**
 * Classe responsável por criar objetos do tipo aresta;
 * @author Bruno R, Joao A, Michel S, Gustavo R.
 */
public class Aresta {
    private Vertice aresta[];
    
    /**
     * Construtor que recebe dois objetos vertice, representando dois pontos de
     * ligação;
     * @param a Vértice inicial;
     * @param b Vértice final;
     */
    public Aresta(Vertice a, Vertice b) {
        this.aresta = new Vertice[2];
        this.aresta[0]=a;
        this.aresta[1]=b;
    }
    
    /**
     * Getter para o Vertice Inicial;
     * @return vértice que compôe a aresta;
     */
    public Vertice getVerticeA() {
        return aresta[0];
    }
    
    /**
     * Getter para o Vertice final;
     * @return vértice que compôe a aresta;
     */
    public Vertice getVerticeB(){
        return aresta[1];
    }    
}
