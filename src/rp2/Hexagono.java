/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rp2;

/**
 *Classe que instancia o hexagono.
 * 
 * @author Bruno R.
 */
public class Hexagono extends PoligonoRegular{

    public Hexagono( double tLado, String CorBorda, String CorPreenchimento, Vertice central) {
        super(6, tLado, CorBorda, CorPreenchimento, central);
    }
}
